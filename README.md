# Pigheat_transcriptome
This repertory contains as of today the scripts and graphics for the analysis of the Crossbred Large white and Creole pig transcriptome used for the Pig Heat project. The files and data are based on the [previous work](https://forgemia.inra.fr/genepi/analyses/pig-heat-scripts) on the project. The existing files were processed and complemented during Arthur Durante's internship in the GenEpi team from January to July 2023 under the supervision of Guillaume Devailly.

## Summary of the different scripts and files used
* [Making R environment](#Making R environment)
* [renv](#renv)
  * [Setting up renv](#Setting up renv)
  * [renv basics](#renv basics)
* [R scripts](#R scripts) 
  * [01_metadata_cleaning](#01_metadata_cleaning)
  * [02_transcriptome normalisation](#02_transcriptome normalisation)

## Making R environment

## renv

renv is a R package made by [Kevin Ushey](https://rstudio.github.io/renv/), as a way to share and keep track of a **r**reproductible **env**ironment within a R project.
renv therefore uses a cache called `renv.lock` in which every package used within a project can be stored, and used throughout multiple scripts without the need to re-install each of them.
renv can be considered as particularly easy to use, and would be beneficial for group projects.

### Setting up renv

### renv basics

each commands of renv is easy to take a hand of, with its syntax always starting with `renv::*command*`.


## R scripts

#### 01_metadata_cleaning

Metadata files containing the different information regarding the animal conditions during sampling, as well as the probes set withing the DNA microarray (**GPL16524** by **GeT-TRiX**) used for transcriptome analysis were taken directly from an [already assembled set](https://forgemia.inra.fr/genepi/analyses/pig-heat-scripts/-/blob/master/6.9_clean_transcripto_md.R), set up by Guillaume Devailly, with the name set as `data/transripto/trx_metadata_GD.tsv`, with the informations as follows :

| trix_name | animal (animal ID) | cond (sampling condition) | sex | bande | trix_batch | data_file |
| --------- |  ----------------- | ------------------------- | --- | ----- | ------------------------------- | --------- |
| *probe position on microarray* |  *animal ID* | *24°C*, *30°C (48 hours within heat stress)*, *30°C (2 weeks within heat stress)* or *tropical* | *Female* or *Castrated (Male)* | *stripe* | *experimental batch of the sample* | *file name containing diverse informations on the animal* |


#### 02_transcriptome normalisation

The existing data were taken from already existing files (as explained above). A new file containing the updated annotations for the microarray probes , implemented by [insert reference], was added, in order to add more data to the set (containing 60305 probes data).
